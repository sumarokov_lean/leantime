package auth

import (
	"LeanTime/app/db"
	"LeanTime/app/helpers"
	"LeanTime/models/User"
	"net/http"
)

func Registration(w http.ResponseWriter, req *http.Request) {
	db_conn := db.Connect()
	defer db_conn.Close()

	var checked_user user.User

	login := req.FormValue("login")
	passwd := helpers.CryptoPassword(req.FormValue("password"))
	db_conn.Where(&user.User{Login: login}).First(&checked_user)

	answer := make(map[string]interface{})
	if checked_user.Exists() {
		answer["is_user_in_DB"] = true
	} else {
		new_user := user.User{Login: login, Password: passwd}
		db_conn.Save(&new_user)
		answer["is_user_in_DB"] = false
		answer["user_id"] = new_user.ID
	}
	helpers.ResponseJSON(w, answer)
}
