package helpers

import (
	"crypto/sha1"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
)

func StrToUint(str string) uint {
	id, err := strconv.ParseUint(str, 10, 32)
	if err != nil {
		fmt.Println(err)
	}
	return uint(id)
}

func ToJSON(data interface{}) []byte {
	result, err := json.Marshal(data)
	if err != nil {
		panic(err.Error())
	}
	return result
}

/** преобразует данные в json и записывает их для отправки клиенту
* @param w {http.ResponseWriter}
* @param data {interface{}} - данные, которые необходимо преобразовать и отправить */
func ResponseJSON(w http.ResponseWriter, data interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.Write(ToJSON(data))
}

func CryptoPassword(pwd string) string {
	sha_pass := sha1.Sum([]byte(pwd))
	password := fmt.Sprintf("%x", sha_pass)
	return password
}
