package process_type

import (
	"LeanTime/app/db"
)

func init() {
	conn := db.Connect()
	conn.AutoMigrate(&ProcessType{})
	fillProcessTypes()
}

type ProcessType struct {
	ID    uint
	Color string
	Name  string
}

/** Наполняет таблицу Типов процессов*/
func fillProcessTypes() {
	conn := db.Connect()
	defer conn.Close()

	process_types := [3]ProcessType{
		{ID: 1, Color: "green", Name: "Полезный"},
		{ID: 2, Color: "yellow", Name: "Неизбежный"},
		{ID: 3, Color: "red", Name: "Бесполезный"},
	}
	for _, process_type := range process_types {
		conn.Create(&process_type)
	}
}
