package auth

import (
	"LeanTime/app/db"
	"LeanTime/app/helpers"
	"LeanTime/models/User"
	"net/http"
)

func Login(w http.ResponseWriter, req *http.Request) {
	db_conn := db.Connect()
	defer db_conn.Close()

	login := req.FormValue("login")
	var checked_user user.User
	db_conn.Where(&user.User{Login: login}).First(&checked_user)

	var user_id uint
	var is_enter bool
	var alert_type string = "warning"
	var alert_message string
	if checked_user.Exists() {
		if passwd := helpers.CryptoPassword(req.FormValue("password")); checked_user.Password == passwd {
			is_enter = true
			user_id = checked_user.ID
		} else {
			alert_type = "error"
			alert_message = "Неверное сочетание Email / Пароль"
		}
	} else {
		alert_message = "Пользователь с email: " + login + " не зарегистрирован"
	}

	answer := make(map[string]interface{})
	answer["user_id"] = user_id
	answer["enter"] = is_enter
	answer["alert_type"] = alert_type
	answer["alert_message"] = alert_message

	helpers.ResponseJSON(w, answer)
}
