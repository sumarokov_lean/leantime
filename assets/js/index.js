require("../css/style.css");

window.moment = require("moment");
window._ = require("lodash");
window.$ = require("jquery");

window.d3 = require("d3");

import Timeperiod from "./classes/Timeperiod.js"
window.Timeperiod = Timeperiod

import {router, Vue} from "./router.js";
import Vuetify from "vuetify";
import ru from 'vuetify/es5/locale/ru'
Vue.use(Vuetify, {
	lang: {
		locales: { ru },
		current: "ru"
	}
});
import "vuetify/dist/vuetify.min.css";

import MyNavbar from "./components/Navbar.vue"
import store from "./vuex.js";
const App = new Vue({
	el: "#app",
	store,
	router,
	components: {
		"my-navbar": MyNavbar,
	},
	mounted() {
		store.commit("appDate", moment().format("YYYY-MM-DD"))
		store.commit("getPeriodTypes");
		store.commit("getProcessTypes");
	}
})
