package config

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"reflect"
)

type Config struct {
	AppName    string
	Version    string
	Port       string
	NameDB     string
	UserDB     string
	PasswordDB string
}

var config_param_names = []string{
	"AppName", "Version", "Port", "NameDB", "UserDB", "PasswordDB",
}

func init() {
	setEnvParams()
}

/** записывает параметры в env */
func setEnvParams() {
	config_params := reflect.ValueOf(readConfigFile())
	for _, param_name := range config_param_names {
		os.Setenv(param_name, config_params.FieldByName(param_name).String())
	}
}

/** читает параметры конфигурации из файла
* @return {Config} - структура с параметрами */
func readConfigFile() Config {
	var config Config
	config_file, err := ioutil.ReadFile("./app/config/config.json")
	if err != nil {
		panic(err.Error())
	}
	err = json.Unmarshal(config_file, &config)
	if err != nil {
		panic(err.Error())
	}
	return config
}
