package routes

import (
	"LeanTime/controllers/Auth"
	"LeanTime/controllers/ProcessType"
	"LeanTime/controllers/Timeperiod"
	"LeanTime/controllers/TimeperiodType"
	"net/http"
)

var Routes []Route

func init() {
	api := InitRoute("api", nil)
	api = api.Group(
		InitRoute("user", nil).Group(
			InitRoute("login", auth.Login),
			InitRoute("registration", auth.Registration),
		),
		InitRoute("process-types", process_type.All),
		InitRoute("timeperiod-types", timeperiod_type.All),
		InitRoute("timeperiods", timeperiod.UniqueNames).Group(
			InitRoute("day", timeperiod.ByDay),
			InitRoute("create", timeperiod.Create),
			InitRoute("copy", timeperiod.Copy),
			InitRoute("delete", timeperiod.Delete),
			InitRoute("find-name", timeperiod.FindName),
			InitRoute("days-with-timeperiod", timeperiod.DaysWithTimeperiod),
		),
	)

	Routes = append(Routes, api)
}

type Route struct {
	name     string
	path     string
	handler  http.HandlerFunc
	parent   *Route
	children map[string]Route
}

func InitRoute(name string, handler http.HandlerFunc) Route {
	return Route{name: name, path: "/" + name, handler: handler}
}

func (r Route) Group(routes ...Route) Route {
	r.children = make(map[string]Route)
	for _, route := range routes {
		route.parent = &r
		route.path = route.parent.path + route.path
		r.children[route.name] = route
	}
	return r
}

func (r Route) Name() string {
	return r.name
}

func (r Route) Handler() http.HandlerFunc {
	return r.handler
}

func (r Route) Parent() *Route {
	return r.parent
}

func (r Route) Children() map[string]Route {
	return r.children
}

func (r Route) Path() string {
	return r.path
}

func (r *Route) SetPath(path string) {
	r.path = path
}
