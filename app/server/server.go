package server

import (
	_ "LeanTime/app/db"
	"LeanTime/routes"
	"fmt"
	"html/template"
	"net/http"
	"os"
)

var LeanTimeServer Server

func init() {
	LeanTimeServer.routes = routes.Routes
	LeanTimeServer.RegRoutesHandlers()
	LeanTimeServer.Run()
}

type Server struct {
	name   string
	routes []routes.Route
}

func (s Server) Run() {
	port := os.Getenv("Port")
	fmt.Printf("Server run on %s\n", port)

	// подключаем файл app.js из папки public
	http.Handle("/public/", http.StripPrefix("/public/", http.FileServer(http.Dir("./public/"))))
	http.HandleFunc("/", render)
	http.ListenAndServe(port, nil)
}

func (s Server) RegRoutesHandlers() {
	for _, route := range s.routes {
		initHandler(&route, "")
	}
}

func initHandler(route *routes.Route, path string) {
	path = path + "/" + route.Name()
	route.SetPath(path)

	if route.Handler() != nil {
		http.HandleFunc(path, route.Handler())
	}

	for _, child_route := range route.Children() {
		initHandler(&child_route, path)
	}
}

func render(w http.ResponseWriter, req *http.Request) {
	tmpl := template.New("./views/index.html").Delims("<<", ">>") // меняем делимитеры в шаблоне Go, т.к. Vue использует "{{ }}"
	tmpl, err := tmpl.ParseFiles("./views/index.html")
	if err != nil {
		fmt.Println(err.Error())
	}
	tmpl.ExecuteTemplate(w, "content", nil)
}
