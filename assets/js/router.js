import Vue from "vue";
import VueRouter from "vue-router";
Vue.use(VueRouter);

import IndexPage from "./views/IndexPage.vue"
import Registration from "./views/Auth/Registration.vue"
import Login from "./views/Auth/Login.vue"

import IndexLeanTime from "./views/MyLeantime/"
import Page404 from "./views/Page404.vue"

const routes = [
	{ "name": "main", "path": "/", "component": IndexPage },
	{ "name": "registration", "path": "/registration", "component": Registration, "meta": { "auth": false } },
	{ "name": "login", "path": "/login", "component": Login, "meta": { "auth": false } },

	{ "path": "/lean-time", "component": IndexLeanTime, "meta": { "auth": true }},
	{ "path": "*", "component": Page404, "meta": { "auth": false }  }
]

const router = new VueRouter({ 
	mode: "history",
	routes 
})

router.beforeEach((to, from, next) => {
	const auth = !!localStorage.getItem("lean_time_user");
	if (to.matched.some(record => record.meta.auth)) {
		if (!auth) {
			next("login")
		} else {
			next()
		}
	} else {
		next()
	}
})

export {router, Vue}
