package timeperiod

import (
	"LeanTime/app/db"
	"LeanTime/app/helpers"
	"LeanTime/models/Timeperiod"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"
)

func UniqueNames(w http.ResponseWriter, req *http.Request) {
	db_conn := db.Connect()
	defer db_conn.Close()

	timeperiods := []timeperiod.Timeperiod{}

	timeperiod_type_id := timeperiod.TypeIdByName(req.FormValue("timeperiod_type"))
	subquery := "SELECT MAX(id) FROM timeperiods WHERE timeperiod_type_id = ? GROUP BY name"

	user_id := req.FormValue("user_id")
	db_conn.Raw("SELECT * FROM timeperiods WHERE user_id = ? AND id IN ("+subquery+") ORDER BY DATE_FORMAT(date_start, '%H:%i')", user_id, timeperiod_type_id).Scan(&timeperiods)

	helpers.ResponseJSON(w, timeperiods)
}

/** при вводе названия периода - ищем похожие в БД */
func FindName(w http.ResponseWriter, req *http.Request) {
	db_conn := db.Connect()
	defer db_conn.Close()

	name := req.FormValue("name")
	user_id := req.FormValue("user_id")
	var timeperiods []timeperiod.Timeperiod
	db_conn.Where("name LIKE ? AND user_id = ?", name+"%", user_id).Select("name").Find(&timeperiods)

	var names []string
	for _, timeperiod := range timeperiods {
		names = append(names, timeperiod.Name)
	}

	helpers.ResponseJSON(w, names)
}

func ByDay(w http.ResponseWriter, req *http.Request) {
	db_conn := db.Connect()
	defer db_conn.Close()

	day := req.FormValue("day")
	user_id := req.FormValue("user_id")

	var timeperiods []timeperiod.Timeperiod
	db_conn.Where("user_id = ? AND (date_start like ? OR date_end like ?)", user_id, day+"%", day+"%").
		Order("date_start").
		Find(&timeperiods)

	helpers.ResponseJSON(w, timeperiods)
}

func Create(w http.ResponseWriter, req *http.Request) {
	db_conn := db.Connect()
	defer db_conn.Close()

	timeperiod := fillTimeperiodByRequest(req)
	db_conn.Create(&timeperiod)

	helpers.ResponseJSON(w, timeperiod)
}

func Delete(w http.ResponseWriter, req *http.Request) {
	db_conn := db.Connect()
	defer db_conn.Close()

	db_conn.Where("id = ?", req.FormValue("ID")).Unscoped().Delete(timeperiod.Timeperiod{})
}

/** возвращает массив дат, в которых есть искомый период */
func DaysWithTimeperiod(w http.ResponseWriter, req *http.Request) {
	db_conn := db.Connect()
	defer db_conn.Close()

	timeperiod_name := req.FormValue("name")
	user_id := req.FormValue("user_id")

	var timeperiods []timeperiod.Timeperiod
	db_conn.Where("name = ? AND user_id = ?", timeperiod_name, user_id).Group("date_start").Find(&timeperiods)

	var days []string
	for _, timeperiod := range timeperiods {
		date := timeperiod.DateStart.Format("2006-01-02")
		days = append(days, date)
	}
	helpers.ResponseJSON(w, days)
}

/** копирует временной период на выбранные дни */
func Copy(w http.ResponseWriter, req *http.Request) {
	type JsonTimeperiod struct {
		Name, TimeStart, TimeEnd                string
		ProcessTypeID, TimeperiodTypeID, UserID uint
	}
	var json_timeperiod JsonTimeperiod
	err := json.Unmarshal([]byte(req.FormValue("timeperiod")), &json_timeperiod)
	if err != nil {
		fmt.Println(err)
	}

	db_conn := db.Connect()
	defer db_conn.Close()

	days_to_copy := strings.Split(req.FormValue("days_to_copy"), ",")
	for _, day_to_copy := range days_to_copy {
		date_start, _ := time.Parse("2006-01-02 15:04", day_to_copy+" "+json_timeperiod.TimeStart)
		date_end, _ := time.Parse("2006-01-02 15:04", day_to_copy+" "+json_timeperiod.TimeEnd)

		timeperiod := timeperiod.Timeperiod{
			Name:             json_timeperiod.Name,
			Description:      "",
			ProcessTypeID:    json_timeperiod.ProcessTypeID,
			TimeperiodTypeID: json_timeperiod.TimeperiodTypeID,
			DateStart:        date_start,
			DateEnd:          date_end,
			UserID:           json_timeperiod.UserID,
		}
		db_conn.Create(&timeperiod)
	}
	helpers.ResponseJSON(w, "Скопировано")
}

func fillTimeperiodByRequest(req *http.Request) timeperiod.Timeperiod {
	process_type_id := helpers.StrToUint(req.FormValue("process_type_id"))
	timeperiod_type_id := helpers.StrToUint(req.FormValue("type_id"))
	user_id := helpers.StrToUint(req.FormValue("user_id"))

	date_start, _ := time.Parse("2006-01-02 15:04", req.FormValue("start_date")+" "+req.FormValue("start_time"))
	date_end, _ := time.Parse("2006-01-02 15:04", req.FormValue("end_date")+" "+req.FormValue("end_time"))

	return timeperiod.Timeperiod{
		Name:             req.FormValue("name"),
		Description:      req.FormValue("description"),
		ProcessTypeID:    process_type_id,
		TimeperiodTypeID: timeperiod_type_id,
		DateStart:        date_start,
		DateEnd:          date_end,
		UserID:           user_id,
	}
}
