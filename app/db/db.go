package db

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"os"
)

func Connect() *gorm.DB {
	db, err := gorm.Open("mysql", os.Getenv("UserDB")+":"+os.Getenv("PasswordDB")+"@/"+os.Getenv("NameDB")+"?parseTime=true")
	if err != nil {
		panic(err.Error())
	}
	return db
}
