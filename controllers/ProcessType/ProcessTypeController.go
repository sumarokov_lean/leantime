package process_type

import (
	"LeanTime/app/db"
	"LeanTime/app/helpers"
	"LeanTime/models/ProcessType"
	"net/http"
)

func All(w http.ResponseWriter, req *http.Request) {
	db_conn := db.Connect()
	defer db_conn.Close()

	var types []process_type.ProcessType
	db_conn.Find(&types)
	helpers.ResponseJSON(w, types)
}
