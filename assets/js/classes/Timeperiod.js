class Timeperiod {

	constructor(timeperiod = {
		ID: null,
		Name: "",
		DateStart: "",
		DateEnd: "",
		Description: "",
		ProcessTypeID: null,
		TimeperiodTypeID: null,
		UserID: null
	}) {
		this.ID = timeperiod.ID
		this.Name = timeperiod.Name
		this.DateStart = timeperiod.DateStart
		this.DateEnd = timeperiod.DateEnd
		this.Description = timeperiod.Description
		this.ProcessTypeID = timeperiod.ProcessTypeID
		this.TimeperiodTypeID = timeperiod.TimeperiodTypeID
		this.UserID = timeperiod.UserID

		this.full_format = "YYYY-MM-DD HH:mm:ss"
	}

	FormatedDate(start_or_end = "Start", format = null) {
		var date = this["Date"+start_or_end]
		format = _.isNil(format) ? this.full_format : format

		return moment(date).utcOffset(date).format(format)
	}

	Duration(measure = "seconds") {
		return moment(this.DateEnd).diff(moment(this.DateStart), measure)
	}

	IsPlan() {
		return this.TimeperiodTypeID == 1
	}

	IsFact() {
		return this.TimeperiodTypeID == 2
	}

	TypeName(lang = "en") {
		var names = {
			1: {ru: "Планируемый", en: "plan"},
			2: {ru: "Фактический", en: "fact"},
		}

		return names[this.TimeperiodTypeID][lang]
	}

	Color() {
		var colors = {
			1: "green",
			2: "yellow",
			3: "red"
		}

		return colors[this.ProcessTypeID]
	}

	ProcessName(lang = "ru") {
		var names = {
			1: {ru: "Полезный", en: "useful"},
			2: {ru: "Неизбежный", en: "inevitable"},
			3: {ru: "Бесполезный", en: "muda"},
		}

		return names[this.ProcessTypeID][lang]
	}

	IsCurrent() {
		let date_format = this.full_format, 
			now = moment().format(date_format),
			date_start = moment.utc(this.DateStart).format(date_format),
			date_end = moment.utc(this.DateEnd).format(date_format);

		return moment(now).isBetween(date_start, date_end)
	}

	InfoStr(time_format = null) {
		var format = _.isNil(time_format) ? this.full_format : time_format
		var timeperiod_info = ""
		timeperiod_info += this.Name + ": "
		timeperiod_info += moment.utc(this.DateStart).format(format) + " - "
		timeperiod_info += moment.utc(this.DateEnd).format(format)

		return timeperiod_info
	}

}

export default Timeperiod
