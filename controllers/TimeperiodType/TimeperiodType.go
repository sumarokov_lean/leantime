package timeperiod_type

import (
	"LeanTime/app/db"
	"LeanTime/app/helpers"
	"LeanTime/models/TimeperiodType"
	"net/http"
)

func All(w http.ResponseWriter, req *http.Request) {
	db_conn := db.Connect()
	defer db_conn.Close()

	var timeperiod_types []timeperiod_type.TimeperiodType
	db_conn.Find(&timeperiod_types)
	helpers.ResponseJSON(w, timeperiod_types)
}
