package user

import (
	"LeanTime/app/db"
	"github.com/jinzhu/gorm"
)

func init() {
	conn := db.Connect()
	conn.AutoMigrate(&User{})
}

type User struct {
	gorm.Model
	Login    string `gorm:"unique"`
	Password string
}

func (user User) Exists() bool {
	return user.ID != 0
}
