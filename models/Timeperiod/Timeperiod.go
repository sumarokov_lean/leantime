package timeperiod

import (
	"LeanTime/app/db"
	"github.com/jinzhu/gorm"
	"time"
)

var PLAN = 1
var FACT = 2

func init() {
	conn := db.Connect()
	conn.AutoMigrate(&Timeperiod{}).Model(&Timeperiod{}).
		AddForeignKey("user_id", "users(id)", "CASCADE", "CASCADE").
		AddForeignKey("process_type_id", "process_types(id)", "CASCADE", "CASCADE").
		AddForeignKey("timeperiod_type_id", "timeperiod_types(id)", "CASCADE", "CASCADE")
}

type Timeperiod struct {
	gorm.Model
	Name             string `gorm:"not null"`
	Description      string
	DateStart        time.Time `gorm:"not null"`
	DateEnd          time.Time `gorm:"not null"`
	ProcessTypeID    uint      `gorm:"not null"`
	TimeperiodTypeID uint      `gorm:"not null"`
	UserID           uint      `gorm:"not null"`
}

func TypeIdByName(type_name string) int {
	var timeperiod_type_id int
	if type_name == "plan" {
		timeperiod_type_id = PLAN
	} else if type_name == "fact" {
		timeperiod_type_id = FACT
	}
	return timeperiod_type_id
}
