package timeperiod_type

import (
	"LeanTime/app/db"
)

func init() {
	conn := db.Connect()
	conn.AutoMigrate(&TimeperiodType{})
	fillTimeperiodTypes()
}

type TimeperiodType struct {
	ID          uint
	Name        string
	ServiceName string
}

/** Наполняет таблицу Типов временнЫх периодов */
func fillTimeperiodTypes() {
	conn := db.Connect()
	defer conn.Close()

	timeperiod_types := [2]TimeperiodType{
		{ID: 1, Name: "Планируемый", ServiceName: "plan"},
		{ID: 2, Name: "Фактический", ServiceName: "fact"},
	}
	for _, timeperiod_type := range timeperiod_types {
		conn.Create(&timeperiod_type)
	}
}
