import {Vue} from "./router.js"
import Vuex from "vuex";
Vue.use(Vuex);

// расскомментировать для продакшена
Vue.config.devtools = false
Vue.config.debug = false
Vue.config.silent = true

const store = new Vuex.Store({
	state: {
		app_date: null,
		timeperiods: [],
		process_types: [],
		period_types: [],
		timeperiod_dialog: false,
		dialog_timeperiod_type: "plan",
	},
	getters: {
		processType: state => id => {
			return state.process_types.find(period => period.Id == id)
		},
		periodType: state => id => {
			return state.period_types.find(period => period.Id == id)
		},
		/** считает сколько секунд затрачено на разные виды периодов 
		 * green - полезное время
		 * yellow - неизбежное 
		 * red - потерянное 
		 * no_type - нераспределенное время */
		seconds(state) {
			var seconds = {
				plan: {green: 0, yellow: 0, red: 0, no_type: 86400},
				fact: {green: 0, yellow: 0, red: 0, no_type: 86400}
			}

			state.timeperiods.forEach(function(timeperiod) {
				seconds[timeperiod.TypeName()][timeperiod.Color()] += timeperiod.Duration()
				seconds[timeperiod.TypeName()].no_type -= timeperiod.Duration()
			})
			return seconds
		},
	},
	mutations: {
		timeperiodDialog(state, params) {
			state.timeperiod_dialog = params.show;
			state.dialog_timeperiod_type = params.timeperiod_type
		},
		appDate(state, date) {
			state.app_date = date;
		},
		pushTimeperiod(state, new_period) {
			var new_timeperiod = new Timeperiod(new_period)
			state.timeperiods.push(new_timeperiod)
		},
		getTimeperiodsByDay(state, day = null) {
			day = _.isNil(day) ? moment().format("YYYY-MM-DD") : day
			state.timeperiods = []
			$.ajax({
				url: "/api/timeperiods/day",
				method: "post",
				data: { "day": day, "user_id": localStorage.lean_time_user },
				success: function(timeperiods) {
					var timeperiods_by_day = []
					timeperiods.forEach(function(timeperiod) {
						timeperiods_by_day.push(new Timeperiod(timeperiod))
					})
					state.timeperiods = timeperiods_by_day
				}
			})
		},
		getProcessTypes(state) {
			$.ajax({
				url: "/api/process-types",
				success: function(process_types) {
					state.process_types = _.toArray(process_types);
				},
				error: function(error) {
					console.log(error)
				}
			})
		},
		getPeriodTypes(state) {
			$.ajax({
				url: "/api/timeperiod-types",
				success: function(period_types) {
					state.period_types = _.toArray(period_types);
				},
				error: function(error) {
					console.log(error)
				}
			})
		}
	}
})

export default store
